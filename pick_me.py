# Please make sure your code is not only correct but also well written, follows python best practices
# and standards and is production ready


# 1) Refactor This code
def _get_bundled_products(product_objs):
    # Returns all bundled products
    bundled_products = []
    for product in product_objs:
        for bundled_product in product.bundled.all():
            bundled_product.price = 0
            bundled_products.append(bundled_product)
    return bundled_products


def _set_normal_products_dict_key(products_dict, product, key):
    # Sets/updates products_dict['key'] in normal_flow
    aux_product = products_dict.get(key, {})
    aux_product.setdefault("products", []).append(mark_safe(key))
    aux_product["price"] = products_dict[key].get("price", 0) + product["net_price"]
    return aux_product


def _set_alternative_products_dict_key(products_dict, product, key):
    # Sets/updates products_dict['key'] in alternative_flow
    aux_product = products_dict.get(
        key, {"expire_in": product.expire_in, "never_expire": product.never_expire}
    )
    aux_product.setdefault("products", []).append(mark_safe(product.name))
    aux_product["price"] = products_dict[key].get("price", 0) + product.price
    return aux_product


def normal_flow(products):
    # Follows the flow for True in original if statement and returns product_list
    products_dict = {}
    for product in products:
        key = product["name"]
        products_dict[key] = _set_normal_products_dict_key(products_dict, product, key)
    return products_dict


def alternative_flow(products):
    # Follows the flow for False in original if statement and returns product_list
    lang = get_language()[:2]
    product_objs = list(
        Product.objects.using("slave")
        .in_bulk([p["product_id"] for p in products])
        .values()
    )
    product_objs.extend(_get_bundled_products(product_objs))
    products_dict = {}
    for product in product_objs:
        key = getattr(product.parent, "name_%s" % lang)
        products_dict[key] = _set_alternative_products_dict_key(
            products_dict, product, key
        )
    return products_dict


def get_products_dict(products=None):
    """
    Returns products dict with bundled products included to be used in email templates.
    NOTE: DO NOT SAVE PRODUCT IN THIS METHOD, AS, WE ARE CHANGING PRICE OF
    BUNDLED PRODUCTS TO 0
    """
    try:
        is_normal_flow = products and products[0].get("source") == "vas"
        products_dict = (
            normal_flow(products) if is_normal_flow else alternative_flow(products)
        )
        # Convert it to a format which is easy to handle in email templates
        products_dict = [
            {"title": key, "body": value} for key, value in products_dict.items()
        ]
    except (ValueError, KeyError, AttributeError):
        products_dict = list(
            {"title": p["name"], "body": {"expire_in": None, "never_expire": None}}
            for p in products
        )
    return products_dict


# 2) For the next two functions, what do you think would go wrong
# with the following approach:
def get_full_name(person):
    full_name = person.pop("first_name") + person.pop("last_name")
    # There is no space between first and last name
    # person dictionary will no longer have first_name and last_name
    person["full_name"] = full_name
    return person


def print_employees_names(employees):
    for employee in employees:
        employee = get_full_name(employee)
        print(employee["full_name"], employee["occupation"])


# 3) How can we improve the following method
import requests, json


def fetch_and_save(url):
    try:
        response = requests.get(url)
        response.raise_for_status()
        to_be_saved = json.loads(response.content())
        # Since we are trying to catch ANY kind of exception we might as well handle db exceptions too.
        db_object = Model(**to_be_saved)
        db_object.save()
    except Exception as ex:
        # Let's print a more descriptive exception message
        message = f"An exception of type {type(ex).__name__} occurred.\n Arguments:"
        print(message)
        for arg in ex.args:
            print(f"* {arg}")


# 4) The following code throws a TypeError, but it also updates the
# tuple, can you please explain why. Bonus points if you can make
# it stop throwing TypeError while maintaining the output.
try:
    t = (1, 2, [30, 40])
    t[2] += [50, 60]
except TypeError:
    pass

# Answer: The reference to the list contained in the tuple is not mutable but the list itself IS.